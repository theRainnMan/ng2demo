
import {LifeCycleComponent} from './demos/life-cycle/life-cycle.component';
import {ChangeDetectionComponent} from './demos/change-detection/change-detection.component';
import {ViewEncapsulationComponent} from './demos/view-encapsulation/view-encapsulation.component';
import {FormComponent} from './demos/form/form.component';
import {DirectiveComponent} from './demos/directive/directive.component';
import {PipeComponent} from './demos/pipe/pipe.component';
import {ComponentCommunicationComponent} from './demos/component-communication/component-communication.component';
import {ObservableComponent} from './demos/observable/observable.component';
import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {LoginComponent} from './demos/auth/login/login.component';
import {AppGuard} from './app.guard';
import {LogoutComponent} from './demos/auth/logout/logout.component';
import {RegisterComponent} from './demos/auth/register/register.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: '',
    canActivate: [AppGuard],
    children: [
        {
          path: 'lc',
          component: LifeCycleComponent
        },
        {
          path: 'cd',
          component: ChangeDetectionComponent
        },
        {
          path: 've',
          component: ViewEncapsulationComponent
        },
        {
          path: 'f',
          component: FormComponent
        },
        {
          path: 'd',
          component: DirectiveComponent
        },
        {
          path: 'p',
          component: PipeComponent
        },
        {
          path: 'cc',
          component: ComponentCommunicationComponent
        },
        {
          path: 'o',
          component: ObservableComponent
        },
        {
          path: 'logout',
          component: LogoutComponent
        }
      ]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
