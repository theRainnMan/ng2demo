import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-cc-child',
  templateUrl: './cc-child.component.html',
  styleUrls: ['./cc-child.component.css']
})
export class CcChildComponent implements OnInit, OnChanges {

  @Input('person')
  childPerson;

  @Output()
  onAddAge = new EventEmitter<number>();

  constructor() { }

  childAge: number;

  ngOnInit() {
  }

  ngOnChanges() {
    this.childAge = this.childPerson.age;
  }

  addAge() {
    // this.childPerson.age++;
    this.childAge++;
    this.onAddAge.emit(this.childAge);
  }

}
